const { ApolloServer, makeExecutableSchema } = require('apollo-server');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
console.log(resolvers);
const schema = makeExecutableSchema(
    {
        typeDefs,
        resolvers,
    }
);
const server = new ApolloServer({ schema });

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});